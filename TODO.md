# RESUME Workshop (26.01.2024)

- [x] Clean 'src' folder.
- [x] Add script or instructions to get all the data.
- [ ] Add annotations and commentary on build/criticality scripts.
- [x] Add License
- [x] Add metadata
- [x] Add visualisation/evaluation scripts
- [x] Decide publication media.
- [x] Fill Metadata
- [x] Get rid of prebuilt files
- [x] Replace requirements.txt with environment.yml


# GENERAL

- [ ] Ensure all the numbers are in a reasonable degree of accuracy.
- [ ] Validate all the units of the tutorial.
- [ ] Add criticality for PV and Wind.
- [ ] Add cost for PV and Wind subtechnologies.
- [ ] Clarify which format of data exchange with Criticality team.
- [ ] Validate the spatial join process, so it assigns the correct countries.
- [X] Perform export control clearance
- [X] Add statement of context
- [X] Add feature list
- [X] Add license questionnaire
- [X] Remove prebuild data and publish separately.
- [X] @schm_j33: The installation instructions for setting up the model should be specified in the README.md probably.
