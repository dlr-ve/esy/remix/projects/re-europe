# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# # Multi-objective optimisation of cost and criticality using pareto fronts
#
# Pareto fronts are built by first finding the optimal solution of a normal
# linear program and then relaxing the optimisation by a percentage of
# the optimal objective value.
#
# ## Dependencies
#
# This feature is already implemented in REMix so we have no extra
# dependencies.
# %%
import pandas as pd
from remix.framework.api.instance import Instance
import os

data_folder = "../data"
raw_folder = "../raw"
idx = pd.IndexSlice
m = Instance()
m.datadir = os.path.join(data_folder, "criticality")
# %%
tech_scope = {
    "LFP": "LFP",
    "Redox-Flow": "RedoxFlow",
    "NMC-111": "NMC",
    "Lead-Acid": "LeadAcid",
}
tech_filter = list(tech_scope.keys())
# %%
criticality_data = pd.read_csv(
    os.path.join(raw_folder, "battery_technologies_mass_weighted_sdp.csv")
)
criticality_data = (
    criticality_data.query("year == 2023 and battery_technology in @tech_filter")
    .reset_index()
    .drop(columns=["index"])
)
criticality_data
# %%
accounting_storageUnits = criticality_data.copy().rename(
    columns={"year": "vintage", "mass_weighted_sdp": "perUnitBuild"}
)
accounting_storageUnits["storage_techs"] = criticality_data["battery_technology"].apply(
    lambda x: tech_scope.get(x, None)
)
accounting_storageUnits["indicator"] = (
    accounting_storageUnits["storage_techs"] + "_Crit"
)
accounting_storageUnits["accNodesData"] = "global"
accounting_storageUnits["vintage"] = accounting_storageUnits["vintage"].astype(str)
accounting_storageUnits = accounting_storageUnits.set_index(
    ["indicator", "accNodesData", "storage_techs", "vintage"]
)[["perUnitBuild"]]

m.parameter.add(accounting_storageUnits.round(3), "accounting_storageunits")
accounting_storageUnits
# %%
accounting_indicatorBounds = pd.DataFrame(
    index=pd.MultiIndex.from_product([["global"], ["horizon"], ["Criticality"]])
)

accounting_indicatorBounds.loc[idx["global", :, "Criticality"], "pareto"] = -1
accounting_indicatorBounds = accounting_indicatorBounds.dropna(
    axis=0, how="all"
).fillna(0)
m.parameter.add(accounting_indicatorBounds, "accounting_indicatorbounds")
accounting_indicatorBounds
# %%
accounting_perIndicator2 = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [
            ["Criticality"],
            ["LFP_Crit", "NMC_Crit", "LeadAcid_Crit", "RedoxFlow_Crit"],
            ["global"],
            ["2023"],
        ]
    )
)
accounting_perIndicator2["perIndicator"] = 1

m.parameter.add(accounting_perIndicator2, "accounting_perindicator")

accounting_perIndicator2
# %%
m.write(fileformat="csv", infer=False)
