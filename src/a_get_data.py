# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# # Downloads of raw data
#
# To build this model you need first to download the data. In this script you
# will find helpful tools to do this automatically but be aware that a lot of
# websites have clauses against crawling tools with Python or any other
# language. To avoid issues we use official channels for Zenodo, which needs
# you to have an API key, and for geoBoundaries the GISCO which offer each
# their own REST API.
#
# If for any reason you are unable to run these scripts, here is a table with
# the files you need, where they have to be and where you get them from:
#
# | Description                               | Source                                              | Directory                                     | Instruction                                                                                                 |
# | ----------------------------------------- | --------------------------------------------------- | --------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
# | RE-Europe Static data                     | https://doi.org/10.5281/zenodo.620228               | raw/Static_data/*                             | Download the "RE-Europe_dataset_package_v1-2.zip" and extract the subfolder Static_data into the raw folder |
# | RE-Europe Time series                     | https://doi.org/10.5281/zenodo.620228               | raw/Nodel_TS/*                                | Download the "RE-Europe_dataset_package_v1-2.zip" and extract the subfolder Nodal_TS into the raw folder    |
# | Reference dataset from master thesis      | https://doi.org/10.5281/zenodo.10518713             | raw/martens_technological_reference/*         | Download and extract to martens_technological_reference                                                     |
# | NUTS GeoJSON                              | https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts | raw/NUTS_RG_20M_2021_4326.geojson | Download after selecting the appropiate options from the dropdown menus: Year 2021, format GeoJSON, Geometry type RG and coordinates EPSG:4326 |
# | Kosovo boundaries GeoJSON                 | https://www.geoboundaries.org/countryDownloads.html | raw/geoBoundaries-XKX-ADM0_simplified.geojson | Download the version from 2021 and ADM0, extract only the GeoJSON file                                      |
# | Bosnia and Herzegovina boundaries GeoJSON | https://www.geoboundaries.org/countryDownloads.html | raw/geoBoundaries-BIH-ADM0_simplified.geojson | Download the version from 2013 and ADM0, extract only the GeoJSON file                                      |
# %%
import requests
from pathlib import Path
import json
import wget
import zipfile

# %% [markdown]
# To work with the Zenodo API, you need to either write your API key or add it
# to your environmental variables.
# %%
raw_folder = "../raw"
Path(raw_folder).mkdir(exist_ok=True)
# %% [markdown]
# ## GISCO NUTS GeoJSON
#
# To download the NUTS GeoJSON, you can either go to the webpage and click
# download or use this script, this method is not problematic because the
# Eurostat itself recommends it in their notebooks, example:
# https://github.com/eurostat/happyGISCO/blob/master/notebooks/03_algorithm_layers_overlay.ipynb
#
# Manual download from:
# https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts
# %%
nuts_domain = "nuts"
gisco_rest = "https://gisco-services.ec.europa.eu/distribution/v2"
nuts_file = "NUTS_RG_20M_2021_4326.geojson"
"%s/%s/%s/%s" % (gisco_rest, nuts_domain, "geojson", nuts_file)
# To avoid downloading over and over always check if you already have the file.
if not Path(raw_folder).joinpath(nuts_file).exists():
    geojson_file = requests.get(
        "%s/%s/%s/%s" % (gisco_rest, nuts_domain, "geojson", nuts_file)
    ).json()
    with open(f"{Path(raw_folder).joinpath(nuts_file)}", "w", encoding="utf8") as f:
        json.dump(geojson_file, f)
else:
    print(f"{nuts_file} already exists, skipping download!")
# %% [markdown]
# ## RE-Europe download
#
# The file is available from Zenodo at: https://doi.org/10.5281/zenodo.620228
# This requires you to set up your API credentials from Zenodo, alternatively
# you can extract the files manually and place them in the raw/ folder.
# %%
re_europe_raw = "RE-Europe_dataset_package_v1-2.zip"
if not Path(raw_folder).joinpath(re_europe_raw).exists():
    url = "https://zenodo.org/api/records/999150"
    r = requests.get(url, timeout=15.0)
    js = r.json()
    files = [f for f in js["files"] if f["key"] == "RE-Europe_dataset_package_v1-2.zip"]
    link = files[0]["links"]["self"]
    filename = wget.download(
        f"{link}",
        out=f"{Path(raw_folder).joinpath(re_europe_raw)}",
    )
else:
    print(f"{re_europe_raw} already exists, skipping download!")
# %% [markdown]
# ### Extracting the zip files on the RE-Europe dataset
# %%
z = zipfile.ZipFile(f"{Path(raw_folder).joinpath(re_europe_raw)}")
Path(raw_folder).joinpath("Static_data").mkdir(exist_ok=True)
Path(raw_folder).joinpath("Nodal_TS").mkdir(exist_ok=True)
for file in z.namelist():
    if file.startswith("RE-Europe_dataset_package/Static_data/"):
        if file.endswith(".csv"):
            file_name = (
                Path(raw_folder).joinpath("Static_data").joinpath(Path(file).name)
            )
            if not file_name.exists():
                with open(f"{file_name}", "wb") as f:
                    f.write(z.read(file))
            else:
                print(f"{file_name} already exists, skipping extraction!")
    if file.startswith("RE-Europe_dataset_package/Nodal_TS/"):
        if file.endswith(".csv"):
            file_name = Path(raw_folder).joinpath("Nodal_TS").joinpath(Path(file).name)
            if not file_name.exists():
                with open(f"{file_name}", "wb") as f:
                    f.write(z.read(file))
            else:
                print(f"{file_name} already exists, skipping extraction!")
# %% [markdown]
# ## Thesis reference data download
#
# The file is available from Zenodo at: https://doi.org/10.5281/zenodo.10518713
# %%
reference_raw = "martens_technological_reference.zip"
if not Path(raw_folder).joinpath(reference_raw).exists():
    url = "https://zenodo.org/api/records/10518713"
    r = requests.get(url, timeout=15.0)
    js = r.json()
    files = [
        f for f in js["files"] if f["key"] == "martens_technological_reference.zip"
    ]
    link = files[0]["links"]["self"]
    filename = wget.download(
        f"{link}",
        out=f"{Path(raw_folder).joinpath(reference_raw)}",
    )
else:
    print(f"{reference_raw} already exists, skipping download!")
# %% [markdown]
# ### Extracting the zip files of the technological reference
# %%
z = zipfile.ZipFile(f"{Path(raw_folder).joinpath(reference_raw)}")
Path(raw_folder).joinpath("martens_technological_reference").mkdir(exist_ok=True)
for file in z.namelist():
    file_name = (
        Path(raw_folder)
        .joinpath("martens_technological_reference")
        .joinpath(Path(file).name)
    )
    if not file_name.exists():
        with open(f"{file_name}", "wb") as f:
            f.write(z.read(file))
    else:
        print(f"{file_name} already exists, skipping extraction!")
# %%
# %% [markdown]
# ## Boundaries of non-EU countries
#
# Non-EU countries are not covered by the Eurostat data services.
# That is why we have to get their geography from other sources for coordinate
# allocation.
# For Bosnia and Herzegovina and Kosovo we rely on the geoBoundaries which are
# itself a source for other data repositories like the Human Data Exchange
# from the OCHSA. They provide clear instructions of attribution and API usage.
# %% [markdown]
# ### Boundaries of non-EU countries
# %%
api_url = "https://www.geoboundaries.org/api/current/gbOpen/{}/ADM0/"
# BIH: Bosnia and Herzegovina, XKX: Kosovo
for country in ["BIH", "XKX"]:
    file_name = f"geoBoundaries-{country}-ADM0_simplified.geojson"
    local_path = Path(raw_folder).joinpath(file_name)
    if not local_path.exists():
        resp = requests.get(api_url.format(country))
        if resp.ok:
            download_link = resp.json()["gjDownloadURL"]
            filename = wget.download(
                download_link, out=f"{Path(raw_folder).joinpath(local_path)}"
            )
    else:
        print(f"{local_path} already exists, you are good to go!")
# %% [markdown]
# ## Battery technologies mass weighted SDP download
#
# The file is available from Zenodo at: https://doi.org/10.5281/zenodo.11349385
# %%
# %%
mwsdp_raw = "battery_technologies_mass_weighted_sdp.zip"
if not Path(raw_folder).joinpath(mwsdp_raw).exists():
    url = "https://zenodo.org/api/records/11349385"
    r = requests.get(url, timeout=15.0)
    js = r.json()
    files = [
        f
        for f in js["files"]
        if f["key"] == "battery_technologies_mass_weighted_sdp.zip"
    ]
    link = files[0]["links"]["self"]
    filename = wget.download(
        f"{link}",
        out=f"{Path(raw_folder).joinpath(mwsdp_raw)}",
    )
else:
    print(f"{re_europe_raw} already exists, skipping download!")
# %% [markdown]
# ### Extracting the zip files on the RE-Europe dataset
# %%
z = zipfile.ZipFile(f"{Path(raw_folder).joinpath(mwsdp_raw)}")
for file in z.namelist():
    if file.startswith("battery_technologies_mass_weighted_sdp"):
        if file.endswith(".csv"):
            file_name = Path(raw_folder).joinpath(Path(file).name)
            if not file_name.exists():
                with open(f"{file_name}", "wb") as f:
                    f.write(z.read(file))
            else:
                print(f"{file_name} already exists, skipping extraction!")

# %%
