# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# ## Evaluation of results
# %%
# importing dependencies
from remix.framework.tools.gdx import GDXEval
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
import json

plt.rcParams["font.family"] = "Arial"

result_dir = "results"

# define often-used shortcut
idx = pd.IndexSlice
# %%
# read in the output `*.gdx` file from the optimization in GAMS
results = GDXEval(f"{result_dir}/criticality_martens.gdx")
# %% [markdown]
# ### Pareto front development
# %%
ax1label = "System Cost [bn€]"
ax2label = "Criticality factor"
indicators = results["indicator_accounting"]
objectives = (
    indicators.loc[:, "global", "horizon", ["Criticality", "SystemCost"]]
    .droplevel([1, 2])["value"]
    .unstack()
)
objectives = objectives.reset_index()
objectives["SystemCost"] = objectives["SystemCost"] / 1000000  # Convert to billion euro
objectives["pareto"] = objectives["pareto"].apply(
    lambda x: int(x.replace("pareto", ""))
)
objectives = objectives.rename(
    columns={"SystemCost": ax1label, "Criticality": ax2label, "pareto": "Pareto point"}
)
fig, ax = plt.subplots(figsize=(8, 2.25))
sns.scatterplot(
    data=objectives,
    color="g",
    x=ax1label,
    y=ax2label,
    hue="Pareto point",
    palette="summer",
    ax=ax,
)
plt.grid(color="lightgray")
ax.legend(loc="center left", bbox_to_anchor=(1, 0.5), title="Pareto point")
y_order = math.floor(math.log(objectives[ax2label].max(), 10))
y_upper = round(objectives[ax2label].max(), -y_order) + math.pow(10, y_order)
y_lower = round(objectives[ax2label].min(), -y_order) - math.pow(10, y_order)
ax.set_ylim([y_lower, y_upper])
# %%
with open("../raw/NUTS_RG_20M_2021_4326.geojson") as json_file:
    geojson = json.load(json_file)
nuts_map = {
    "POR": "Portugal",
    "ESP": "España",
    "FRA": "France",
    "BEL": "Belgique/België",
    "CHE": "Schweiz/Suisse/Svizzera",
    "LUX": "Luxembourg",
    "NLD": "Nederland",
    "ITA": "Italia",
    "DEU": "Deutschland",
    "AUT": "Österreich",
    "DNK": "Danmark",
    "CZE": "Česko",
    "POL": "Polska",
    "HUN": "Magyarország",
    "SVK": "Slovensko",
    "SVN": "Slovenija",
    "HRV": "Hrvatska",
    "GRC": "Elláda",
    "ALB": "Shqipëria",
    "MKD": "Severna Makedonija",
    "BGR": "Bulgaria",
    "MNE": "Crna Gora",
    "BIH": "BIH",
    "SRB": "Serbia",
    "ROU": "România",
}
# %%
generation_values = (
    results["commodity_balance_annual"]
    .loc[idx[:, :, :, :, "Elec", "positive"], idx[:]]
    .groupby(["pareto", "accNodesModel", "techs"], observed=False)
    .sum()
    .div(1e3)
    .round(0)
    .unstack()
)
generation_values.columns = generation_values.columns.get_level_values(1)
generation_values.columns.name = None
generation_values = generation_values.reset_index()
generation_values["Generation"] = 0
generation_values["accNodesModel"] = generation_values["accNodesModel"].apply(
    lambda x: nuts_map.get(x, x)
)
generation_values_europe = generation_values[
    (generation_values["accNodesModel"] != "global")
]
# %%
storage_capacities = (
    results["storage_caps"]
    .loc[:, ["BEL", "NLD"], "2023", :, :, "build"]
    .droplevel([-1, -2, -4])
    .reset_index()
)
storage_capacities["pareto"] = storage_capacities["pareto"].apply(
    lambda x: int(x.replace("pareto", ""))
)
storage_capacities["accNodesModel"] = storage_capacities["accNodesModel"].astype(str)
fig = sns.catplot(
    data=storage_capacities,
    x="pareto",
    y="value",
    hue="techs",
    col="accNodesModel",
    kind="bar",
)
fig.set_ylabels("Storage capacity [GWh]")
fig.set_xlabels("Pareto point")
# %%
generation_renewable = (
    results["commodity_balance_annual"]
    .loc[:, ["BEL", "NLD"], "2023", ["PV", "Wind"], :, "positive"]
    .droplevel([2, 4, 5])
    .reset_index()
)
generation_renewable["pareto"] = generation_renewable["pareto"].apply(
    lambda x: int(x.replace("pareto", ""))
)
generation_renewable["accNodesModel"] = generation_renewable["accNodesModel"].astype(
    str
)
generation_renewable["techs"] = generation_renewable["techs"].astype(str)
fig = sns.catplot(
    data=generation_renewable,
    x="pareto",
    y="value",
    col="accNodesModel",
    row="techs",
    kind="bar",
)
fig.set_ylabels("Electricity generation [GWh]")
fig.set_xlabels("Pareto point")
