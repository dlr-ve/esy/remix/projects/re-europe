# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# # Thesis context
#
# This model was conceived in context of the master thesis of Lilli Martens.
# Since then it has been changed considerably yet we wanted to keep her work
# reproducible. This script produces a scenario where the calculation is
# limited to renewable energies and storage units only between the nodes
# Netherlands (NLD) and Belgium (BEL).
# %%
from remix.framework.api.instance import Instance
import pandas as pd

m = Instance()

m.datadir = "../data/martens"

conventionals = [
    "Coal",
    "Gas",
    "Geothermal",
    "Hydro",
    "Lignite",
    "Waste",
    "Nuclear",
    "Oil",
    "Waste",
]

converter_capacityparams = pd.read_csv(
    "../data/converter_capacityparam.csv",
    index_col=["nodesData", "years", "converter_techs"],
)
conventionals = [
    c for c in conventionals if c in converter_capacityparams.index.levels[-1]
]
converter_capacityparams.loc[:, :, conventionals] = 0

m.parameter.add(converter_capacityparams, "converter_capacityparam")
m.set.add(["BEL", "NLD"], "nodesmodelsel")

m.write(fileformat="csv", infer=False)
# %%
m.datadir = "../data/criticality/martens/"
m.write(fileformat="csv", infer=False)
# %%
m.datadir = "../data"
# %%
m.run(
    resultfile="criticality_martens",
    scendir="criticality/martens",
    lo=3,
    names=1,
    postcalc=1,
    roundts=1,
    method="pareto",
    paretopoints=5,
    paretofactor=1.02,
)
