# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# # RE-Europe REMix implementation
#
# In this notebook we intend to utilize the data published by Jensen and Pinson
# on 28.10.2017 to represent our base model. This should also be a general
# example on how REMix models are usually built.
#
# ## Dependencies
#
# For this project we will need the following dependencies. At this point you
# should have already have installed remix.framework. If you haven't, please
# refer to the installation page:
# https://dlr-ve.gitlab.io/esy/remix/framework/dev/getting-started/install-remix.html
# %% [markdown]
# ## Units reference
#
# REMix has no a way to explicity set units as of now, so one has to pay
# attention to consistency to get valuable results.
# This table summarizes what units should be expected in which context.
#
# | Quantity            |  Units  | Comment                                                                |
# | ------------------- | ------- | ---------------------------------------------------------------------- |
# | Energy (electrical) | GWh     | This is the reference value, conversion coefficients often are per GWh |
# | Power (electrical)  | GW      | Power plant capacities are reported based on their output.             |
# | Stored energy       | GWh(ch) | Stored electricity of all technologies                                 |
# | Carbon emissions    | Mt_CO2  | Emissions of conventional power are on Mt per GWh produced             |
# | Costs               | k€      | All cost values are in 1000 Euro                                       |
# | Criticality         | index   | Mass weighted supply disruption probability                           ​ |

# %%
import numpy as np
import pandas as pd
import os
import datetime
import pycountry
from remix.framework.api.instance import Instance

idx = pd.IndexSlice

# %% [markdown]
# ## Initialising a model instance
#
# The REMix core data model consists of a database of tabular data files which
# can be serialised as CSV files and operated with Pandas DataFrames. In this
# case we will use the `Instance` class which offers some functionalities
# practical for notebook and script users like code completion and verbose
# errors.
# %%
m = Instance()
m.datadir = "../data"
raw_folder = "../raw"
# %% [markdown]
# ## Setting up some references
#
# Apart from the base model provided by RE-Europe we will be adding our own
# storage components. These are differentiated by technology type and storage
# rates. These variables will be useful later.
# %%
storage_techs = ["LeadAcid", "NMC", "RedoxFlow", "LFP"]
charge_techs = [s + "_charger" for s in storage_techs]
e2p_storage = ["e2p_" + s for s in storage_techs]
storage_capacity = 2
# %% [markdown]
# ## Assigning spatial and temporal scope
#
# We need this map as support to consistently assign countries to the codes we
# use across the model. This map is mostly referential since we use other
# encoding in the notebooks. We invert the dictionary so the matching can happen
# in both directions.
# %%
nodes_map = {
    "POR": "Portugal",
    "ESP": "Spain",
    "FRA": "France",
    "BEL": "Belgium",
    "CHE": "Switzerland",
    "LUX": "Luxembourg",
    "NLD": "Netherlands",
    "ITA": "Italy",
    "DEU": "Germany",
    "AUT": "Austria",
    "DNK": "Denmark",
    "CZE": "Czech Republic",
    "POL": "Poland",
    "HUN": "Hungary",
    "SVK": "Slovakia",
    "SVN": "Slovenia",
    "HRV": "Croatia",
    "GRC": "Greece",
    "ALB": "Albania",
    "MKD": "Republic of Macedonia",
    "BGR": "Bulgaria",
    "MNE": "Montenegro",
    "BIH": "Bosnia and Herzegovina",
    "SRB": "Serbia",
    "ROU": "Romania",
}
inv_map = {v: k for k, v in nodes_map.items()}
REDUCED_SCOPE = [
    # "DEU",
    # "BEL",
    # "NLD",
]  # ["DEU", "BEL", "NLD"]  # Leave empty to consider the whole model
REDUCED_SCOPE_DATA = []  # Leave always empty, we need the variable
# %% [markdown]
# Using the previous map and the pycountry library we assign node labels to
# each one of the nodes. These labels will be helpful across the model building
# to assign technologies, costs and capacities to each node. To avoid problems
# with data parsing and make it easier to differentiate data sources we turn
# IDs to strings and add a suffix.
#
# Note: There is a discrepancy between the ISO-3166 Alpha-3 codes used by
# pycountry and the code used by the RE-Europe dataset.
# We stick for now with the code "POR" from the original dataset.
# %%
nodes_raw = pd.read_csv(os.path.join(raw_folder, "Static_data/network_nodes.csv"))
nodes_map_val = {
    n: pycountry.countries.get(alpha_3=f"{n}") for n in nodes_raw["country"].unique()
}
nodes_map_val["POR"] = pycountry.countries.get(alpha_3="PRT")
nodes_map_val = {k: v.name for k, v in nodes_map_val.items()}
generator_nodes = pd.read_csv(
    os.path.join(raw_folder, "Static_data/generator_info.csv")
)
# Assign the nodesModel column to their respective code
generator_nodes["nodesModel"] = generator_nodes["country"].apply(
    lambda x: inv_map.get(x, None)
)
# Add "GEN" prefix to all the generator nodes
generator_nodes["nodesData"] = "GEN" + generator_nodes["ID"].astype(str).str.zfill(5)
generator_nodes["aggregate"] = 1
nodes_gen_raw = generator_nodes[["nodesData", "nodesModel", "aggregate"]]
nodes_raw["nodesData"] = nodes_raw["ID"].astype(str).str.zfill(4)
nodes_raw = nodes_raw[["nodesData", "country"]]
nodes_raw = nodes_raw.rename(columns={"country": "nodesModel"})
nodes_raw["nodesData"] = "ID" + nodes_raw["nodesData"].astype(str)
nodes_raw["aggregate"] = 1
network_nodes = list(nodes_raw["nodesData"].unique())
if len(REDUCED_SCOPE_DATA) > 0:
    network_nodes = [n for n in network_nodes if n in REDUCED_SCOPE_DATA]
# %% [markdown]
# ## Aggregation of spatial scopes and accounting indicators
#
# Using the DataFrame that we just built we assign  country codes as targets of
# aggregation of each node. Sets are used as domains for the equations across
# the model. We declare "nodesData" as each of the individual nodes and
# "nodesModel" to the aggregated one. Optimisations happen always at the level
# of "nodesModel", which will be the same as "nodesData" when no mapping is
# supplied. To facilitate the reusability of data the "nodesModelSel" set can
# be used to limit the scope of the solving process.
# %%
df = pd.concat([nodes_raw.copy(), nodes_gen_raw.copy()])
if len(REDUCED_SCOPE) != 0:
    df = df[df["nodesModel"].isin(REDUCED_SCOPE)]
    REDUCED_SCOPE_DATA = df.nodesData.to_list()

df.set_index(["nodesData", "nodesModel"], inplace=True)
df = df.drop(columns="aggregate")

m.map.add(df, "aggregatenodesmodel")

m.set.add(
    list(sorted(set(m.map.aggregatenodesmodel.index.get_level_values(0)))), "nodesdata"
)
m.set.add(
    list(sorted(set(m.map.aggregatenodesmodel.index.get_level_values(1)))), "nodesmodel"
)
m.set.add(
    list(sorted(set(m.map.aggregatenodesmodel.index.get_level_values(1)))),
    "nodesmodelsel",
)
# %% [markdown]
# Similarly we assign the temporal scope by telling the model which years are
# within the scope "years" and which ones should be optimised, the "yearssel".
# %%
m.set.add(["2023"], "years")
m.set.add(["2023"], "yearssel")
# %% [markdown]
# ## Defining objectives and boundary conditions
#
# The variable "SystemCost" is the primary minimisation objective. The
# "e2p_storage" variables stands for the energy-to-power ratio, which ensures
# that the charging and storage capacities of storage technologies is expanded
# with a fixed ratio.
# %%
accounting_indicatorBounds = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [["global"] + network_nodes, ["horizon"], ["SystemCost"] + e2p_storage]
    )
)

accounting_indicatorBounds.loc[idx["global", :, "SystemCost"], "obj"] = -1

accounting_indicatorBounds.loc[idx[network_nodes, :, e2p_storage], "useFixed"] = 1
accounting_indicatorBounds.loc[idx[network_nodes, :, e2p_storage], "fixedValue"] = 0

accounting_indicatorBounds = accounting_indicatorBounds.dropna(
    axis=0, how="all"
).fillna(0)
m.parameter.add(accounting_indicatorBounds, "accounting_indicatorbounds")
accounting_indicatorBounds
# %% [markdown]
# The carbon variable is used to account and constrain the carbon emissions of
# the model.
# %%
accounting_indicatorBoundsCarbonLimit = pd.DataFrame(
    index=pd.MultiIndex.from_product([["global"], ["horizon"], ["carbon"]])
)

accounting_indicatorBoundsCarbonLimit.loc[
    idx["global", "horizon", "carbon"], "useUpper"
] = 1
accounting_indicatorBoundsCarbonLimit.loc[
    idx["global", "horizon", "carbon"], "upperValue"
] = (2.75 * 1000)

m.parameter.add(accounting_indicatorBoundsCarbonLimit, "accounting_indicatorbounds")
accounting_indicatorBoundsCarbonLimit
# %% [markdown]
# Each indicator can be assigned to other indicators for quantifying
# activities and investments.
# %%
accounting_perIndicator = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [
            ["SystemCost"],
            ["Invest", "OMFix", "slackCost", "OMVar", "emissionCost"],
            ["global"],
            m.set.yearssel,
        ]
    )
)
accounting_perIndicator["perIndicator"] = 1

m.parameter.add(accounting_perIndicator, "accounting_perindicator")
accounting_perIndicator
# %% [markdown]
# ## Adding renewable potential
#
# RE-Europe offers two renewable energy installable potential calculations. One
# based on the ECMWF deterministic forecast and another based on the COSMO-REA6
# reanalysis of the European CORDEX EUR-11. The latter has a higher resolution,
# so we opted for using it for this project. We rename the identifiers like we
# did in the node assignments and reorder the data in such a way that it is
# compatible with REMix.
# %%
input_convertercapacity_pv = (
    pd.read_csv(os.path.join(raw_folder, "Static_data/solar_layouts_COSMO.csv"))
    .rename(columns={"node": "nodesData", "Proportional": "unitsUpperLimit"})
    .drop(columns=["Uniform"])
)
input_convertercapacity_pv["vintage"] = "2023"
input_convertercapacity_pv["converter_techs"] = "PV"
input_convertercapacity_pv["unitsLowerLimit"] = 0
input_convertercapacity_pv["nodesData"] = "ID" + input_convertercapacity_pv[
    "nodesData"
].astype(str).str.zfill(4)
input_convertercapacity_pv = input_convertercapacity_pv.set_index(
    ["nodesData", "vintage", "converter_techs"]
)
input_convertercapacity_pv.head()
# %%
input_convertercapacity_wind = (
    pd.read_csv(os.path.join(raw_folder, "Static_data/wind_layouts_COSMO.csv"))
    .rename(columns={"node": "nodesData", "Proportional": "unitsUpperLimit"})
    .drop(columns=["Uniform"])
)
input_convertercapacity_wind["vintage"] = "2023"
input_convertercapacity_wind["converter_techs"] = "Wind"
input_convertercapacity_wind["unitsLowerLimit"] = 0
input_convertercapacity_wind["nodesData"] = "ID" + input_convertercapacity_wind[
    "nodesData"
].astype(str).str.zfill(4)
input_convertercapacity_wind = input_convertercapacity_wind.set_index(
    ["nodesData", "vintage", "converter_techs"]
)
input_convertercapacity_wind.head()
# %% [markdown]
# ## Adding storage technology expansion
#
# Additionaly to the base data, we add to our model the possibility to expand on
# charger capacities. This model scope comprises four different storage
# technologies.
# %%
chargers = ["LFP_charger", "LeadAcid_charger", "NMC_charger", "RedoxFlow_charger"]
charger_capacity = pd.DataFrame(
    index=pd.MultiIndex.from_product([network_nodes, ["2023"], chargers])
)
charger_capacity.index.names = ["nodesData", "vintage", "converter_techs"]
charger_capacity["unitsLowerLimit"] = 0
charger_capacity["unitsUpperLimit"] = np.inf
charger_capacity.head()
# %% [markdown]
# ## Adding conventional power capacities
#
# Conventional power generation is not the main focus of this example. We
# simplify its utilisation by using the original capacities, blocking expansion
# and only accounting costs based on marginals.
# %%
german_nodes = generator_nodes[
    (generator_nodes["country"] == "Germany")
    & (generator_nodes["primaryfuel"] == "Nuclear")
]["nodesData"].unique()
plant_capacity = generator_nodes[["nodesData", "primaryfuel", "capacity"]].rename(
    columns={"primaryfuel": "converter_techs", "capacity": "unitsLowerLimit"}
)
plant_capacity["vintage"] = "2023"
plant_capacity["unitsUpperLimit"] = plant_capacity["unitsLowerLimit"]
plant_capacity["converter_techs"] = (
    plant_capacity["converter_techs"]
    .replace("Fuel Oil", "Oil")
    .replace("Natural Gas", "Gas")
)
plant_capacity = plant_capacity[~plant_capacity["converter_techs"].isin(["Unknown"])]
plant_capacity = plant_capacity.set_index(["nodesData", "vintage", "converter_techs"])
powerplant_techs = list(plant_capacity.index.levels[2])
plant_capacity.head()
# %% [markdown]
# ## Collecting all the capacity constraints
#
# All the capacity data can be collected in the "converter_capacityparam" file
# from REMix.
# %%
converter_capacityParam = (
    pd.concat(
        [
            plant_capacity,
            charger_capacity,
            input_convertercapacity_pv,
            input_convertercapacity_wind,
        ]
    )
    .div(1e3)
    .sort_index()
)
converter_capacityParam.loc[german_nodes, :, :] = 0
if len(REDUCED_SCOPE_DATA) > 0:
    converter_capacityParam = converter_capacityParam.query(
        "nodesData  == @REDUCED_SCOPE_DATA"
    )
m.parameter.add(converter_capacityParam.round(3), "converter_capacityparam")
converter_capacityParam
# %% [markdown]
# ## Technical parameters
#
# Technical parameters like life times and maximum capacity factor are added in
# the "converter_techparam" file. The table is built based on the reference
# dataset. The lifetime of the powerplants was generalised at 30, since we are
# not doing multi-year analyses this is not relevant, anyway.
# This information is found in the reference dataset.
# %%
storage_reference_parameters = pd.read_csv(
    os.path.join(raw_folder, "martens_technological_reference/storage_parameters.csv")
)
renewable_reference_parameters = pd.read_csv(
    os.path.join(raw_folder, "martens_technological_reference/renewable_parameters.csv")
)
renewable_reference_parameters = renewable_reference_parameters[
    renewable_reference_parameters["technology"].isin(["pv", "wind"])
]
renewable_reference_parameters["technology"] = renewable_reference_parameters[
    "technology"
].replace({"pv": "PV", "wind": "Wind"})
# %%
converter_techParamSt = storage_reference_parameters[["technology", "lifetime"]].rename(
    columns={"technology": "converter_techs", "lifetime": "lifeTime"}
)
converter_techParamSt["converter_techs"] = (
    converter_techParamSt["converter_techs"] + "_charger"
)
converter_techParamRe = renewable_reference_parameters[
    ["technology", "lifetime"]
].rename(columns={"technology": "converter_techs", "lifetime": "lifeTime"})
converter_techParam = pd.concat([converter_techParamSt, converter_techParamRe])
converter_techParam["vintage"] = "2023"
converter_techParam["activityUpperLimit"] = 1
converter_techParam["lifeTime"] = converter_techParam["lifeTime"].astype(int)
# converter_techParam["vintage"] = converter_techParam["vintage"].astype(str)
converter_techParam = converter_techParam.set_index(["converter_techs", "vintage"])
pp_techparam = pd.DataFrame(
    index=pd.MultiIndex.from_product([powerplant_techs, ["2023"]])
)
pp_techparam.index.names = ["converter_techs", "vintage"]
pp_techparam["lifeTime"] = 30
pp_techparam["activityUpperLimit"] = 1
m.parameter.add(pp_techparam, "converter_techparam")
m.parameter.add(converter_techParam, "converter_techparam")
converter_techParam
# %% [markdown]
# ## Converter coefficients
#
# Coefficients are a fundamental concept in REMix. They enable its
# multi-commodity evaluation capacities. In this project we utilize emission
# values extracted from the Umweltbundesamt:
# https://www.umweltbundesamt.de/dokument/co2-emissionsfaktorenliste-fuer-energie
# They have a pretty restrictive license so no downloading is possible.
# To validate these values you will have to look them up yourself.
# %%
emission_coefficients = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [
            ["Coal", "Gas", "Lignite", "Oil", "Waste"],
            ["2023"],
            ["Powergen"],
            ["Elec", "CO2"],
        ]
    )
)
emission_coefficients["coefficient"] = 1
emission_coefficients.loc["Waste", :, :, "CO2"] = np.int64(0.298)  # Mt/GWh
emission_coefficients.loc["Oil", :, :, "CO2"] = np.int64(0.285)  # Mt/GWh
emission_coefficients.loc["Lignite", :, :, "CO2"] = np.int64(0.356)  # Mt/GWh
emission_coefficients.loc["Gas", :, :, "CO2"] = np.int64(0.200)  # Mt/GWh
emission_coefficients.loc["Coal", :, :, "CO2"] = np.int64(0.337)  # Mt/GWh
nonemission_coefficients = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [
            ["Nuclear", "Geothermal", "Hydro", "PV", "Wind"],
            ["2023"],
            ["Powergen"],
            ["Elec"],
        ]
    )
)
nonemission_coefficients["coefficient"] = 1
# %%
converter_efficiencies = storage_reference_parameters.loc[
    :, ["technology", "efficiency"]
]
converter_efficiencies.loc[:, "converter_techs"] = (
    converter_efficiencies.loc[:, "technology"] + "_charger"
)
converter_efficiencies.loc[:, "vintage"] = "2023"
storage_coeffiecients_in = converter_efficiencies[
    ["converter_techs", "vintage", "technology", "efficiency"]
].rename(columns={"technology": "commodity", "efficiency": "coefficient"})
storage_coeffiecients_in["coefficient"] = storage_coeffiecients_in["coefficient"] / 100
storage_coeffiecients_in["commodity"] = (
    "Storage_" + storage_coeffiecients_in["commodity"]
)
storage_coeffiecients_in["activity"] = "Charge"
elec_coefficients_in = storage_coeffiecients_in.copy()
elec_coefficients_in["commodity"] = "Elec"
elec_coefficients_in["coefficient"] = -1

storage_coeffiecients_out = converter_efficiencies[
    ["converter_techs", "vintage", "technology", "efficiency"]
].rename(columns={"technology": "commodity", "efficiency": "coefficient"})
storage_coeffiecients_out["coefficient"] = (
    -100 / storage_coeffiecients_out["coefficient"]
)
storage_coeffiecients_out["commodity"] = (
    "Storage_" + storage_coeffiecients_out["commodity"]
)
storage_coeffiecients_out["activity"] = "Discharge"
elec_coefficients_out = storage_coeffiecients_out.copy()
elec_coefficients_out["commodity"] = "Elec"
elec_coefficients_out["coefficient"] = 1
converter_coefficient = (
    pd.concat(
        [
            storage_coeffiecients_in,
            elec_coefficients_in,
            storage_coeffiecients_out,
            elec_coefficients_out,
        ]
    )
    .set_index(["converter_techs", "vintage", "activity", "commodity"])
    .sort_index()
    .round(2)
)
# %%
m.parameter.add(emission_coefficients, "converter_coefficient")
m.parameter.add(nonemission_coefficients, "converter_coefficient")
m.parameter.add(converter_coefficient, "converter_coefficient")
converter_coefficient
# %% [markdown]
# ## Renewable production profiles
#
# Along the previously added renewable potentials, RE-Europe provides
# time series of the renewable energy capacity factors associated to their
# respective sources. To utilize them we parse the dates, align labels and
# transpose the data, so its in line with the REMix format. We define the
# function `hour_of_year()` to use across the rest of the script.
# %%


def hour_of_year(dt: datetime.datetime) -> float:
    """Helper function to calculate the hour of the year froma datetime
    object."""
    beginning_of_year = datetime.datetime(dt.year, 1, 1, tzinfo=dt.tzinfo)
    return (dt - beginning_of_year).total_seconds() // 3600


raw_solarprofile = pd.read_csv(
    os.path.join(raw_folder, "Nodal_TS/solar_signal_COSMO.csv"), parse_dates=["Time"]
)
raw_solarprofile_2013 = raw_solarprofile[raw_solarprofile.Time.dt.year == 2013]
raw_solarprofile_2013.loc[:, "Time"] = raw_solarprofile_2013.loc[:, "Time"].apply(
    lambda x: "t" + str(int(hour_of_year(x)) + 1).zfill(4)
)
raw_solarprofile_2013 = raw_solarprofile_2013.set_index("Time").iloc[:8760].T
raw_solarprofile_2013.index.name = "node"
converter_activityProfile = raw_solarprofile_2013.reset_index()
converter_activityProfile["nodesData"] = (
    converter_activityProfile["node"].astype(str).str.zfill(4)
)
converter_activityProfile = converter_activityProfile.reset_index().drop(
    columns=["node", "index"]
)
converter_activityProfile["nodesData"] = "ID" + converter_activityProfile["nodesData"]
# %%
converter_activityProfile = converter_activityProfile[
    converter_activityProfile["nodesData"].isin(
        list(m.map.aggregatenodesmodel.index.levels[0])
    )
]
# add columns and set them as index
converter_activityProfile["years"] = "2023"
converter_activityProfile["type"] = "upper"
converter_activityProfile["techs"] = "PV"
converter_activityProfile = converter_activityProfile.set_index(
    ["nodesData", "years", "techs", "type"]
)
if len(REDUCED_SCOPE_DATA) > 0:
    converter_activityProfile = converter_activityProfile.query(
        "nodesData  == @REDUCED_SCOPE_DATA"
    )
m.profile.add(converter_activityProfile, "converter_activityprofile")
converter_activityProfile.iloc[:, 0:8]
# %%
raw_windprofile = pd.read_csv(
    os.path.join(raw_folder, "Nodal_TS/wind_signal_COSMO.csv"), parse_dates=["Time"]
)
raw_windprofile_2013 = raw_windprofile[raw_windprofile.Time.dt.year == 2013]
raw_windprofile_2013.loc[:, "Time"] = raw_windprofile_2013.loc[:, "Time"].apply(
    lambda x: "t" + str(int(hour_of_year(x)) + 1).zfill(4)
)
raw_windprofile_2013 = raw_windprofile_2013.set_index("Time").iloc[:8760].T
raw_windprofile_2013.index.name = "node"
converter_activityProfile = raw_windprofile_2013.reset_index()
converter_activityProfile["nodesData"] = (
    converter_activityProfile["node"].astype(str).str.zfill(4)
)
converter_activityProfile = converter_activityProfile.reset_index().drop(
    columns=["node", "index"]
)
converter_activityProfile["nodesData"] = "ID" + converter_activityProfile[
    "nodesData"
].astype(str)
converter_activityProfile = converter_activityProfile[
    converter_activityProfile["nodesData"].isin(
        list(m.map.aggregatenodesmodel.index.levels[0])
    )
]
converter_activityProfile["years"] = "2023"
converter_activityProfile["type"] = "upper"
converter_activityProfile["techs"] = "Wind"
converter_activityProfile = converter_activityProfile.set_index(
    ["nodesData", "years", "techs", "type"]
)
if len(REDUCED_SCOPE_DATA) > 0:
    converter_activityProfile = converter_activityProfile.query(
        "nodesData  == @REDUCED_SCOPE_DATA"
    )
m.profile.add(converter_activityProfile, "converter_activityprofile")
converter_activityProfile.iloc[:, 0:8]
# %% [markdown]
# ## Variable Operation and Maintenance costs
#
# In this model we simplify the operation of conventional power by only
# accounting it based on the marginal costs provided by RE fuels. All costs
# are in k€.
# %%
accounting_converteractivity = generator_nodes[
    ["nodesData", "primaryfuel", "lincost"]
].rename(columns={"primaryfuel": "converter_techs", "lincost": "perActivity"})
accounting_converteractivity["converter_techs"] = (
    accounting_converteractivity["converter_techs"]
    .replace("Fuel Oil", "Oil")
    .replace("Natural Gas", "Gas")
)
accounting_converteractivity["vintage"] = "2023"
accounting_converteractivity["activity"] = "Powergen"
accounting_converteractivity["indicator"] = "OMVar"
accounting_converteractivity = accounting_converteractivity.set_index(
    ["indicator", "nodesData", "converter_techs", "vintage", "activity"]
)
accounting_converteractivity = accounting_converteractivity.round(2)
if len(REDUCED_SCOPE_DATA) > 0:
    accounting_converteractivity = accounting_converteractivity.query(
        "nodesData  == @REDUCED_SCOPE_DATA"
    )
m.parameter.add(accounting_converteractivity, "accounting_converteractivity")
# %%
accounting_converterunitsCh = storage_reference_parameters[
    ["technology", "charger_investment", "lifetime"]
].rename(columns={"charger_investment": "investment_cost"})
accounting_converterunitsCh["technology"] = (
    accounting_converterunitsCh["technology"] + "_charger"
)
accounting_converterunitsRe = renewable_reference_parameters[
    ["technology", "investment_cost", "lifetime"]
]
accounting_converterunitsInv = pd.concat(
    [accounting_converterunitsCh, accounting_converterunitsRe]
)
accounting_converterunitsInv["perUnitBuild"] = (
    accounting_converterunitsInv["investment_cost"] * 1000
)
accounting_converterunitsInv["accNodesData"] = "global"
accounting_converterunitsInv["indicator"] = "Invest"
accounting_converterunitsInv["vintage"] = "2023"
accounting_converterunitsInv["useAnnuity"] = 1
accounting_converterunitsInv["interest"] = 0.06
accounting_converterunitsInv = accounting_converterunitsInv.rename(
    columns={"technology": "storage_techs", "lifetime": "amorTime"}
)
accounting_converterunitsInv = accounting_converterunitsInv[
    [
        "indicator",
        "accNodesData",
        "storage_techs",
        "vintage",
        "perUnitBuild",
        "useAnnuity",
        "amorTime",
        "interest",
    ]
]

accounting_converterunitsCh2 = storage_reference_parameters[
    ["technology", "charger_investment", "om_cost"]
].rename(columns={"charger_investment": "investment_cost"})
accounting_converterunitsCh2["technology"] = (
    accounting_converterunitsCh2["technology"] + "_charger"
)
accounting_converterunitsRe2 = renewable_reference_parameters[
    ["technology", "investment_cost", "om_cost"]
]
# NOTE consult if omfix cost are already total in the storage.
accounting_converterunitsOfix = accounting_converterunitsRe2  # pd.concat(
#     [accounting_converterunitsCh2, accounting_converterunitsRe2]
# )
accounting_converterunitsOfix["perUnitTotal"] = (
    accounting_converterunitsOfix["investment_cost"]
    * 1000
    * accounting_converterunitsOfix["om_cost"]
    / 100
)

accounting_converterunitsOfix["accNodesData"] = "global"
accounting_converterunitsOfix["indicator"] = "OMFix"
accounting_converterunitsOfix["vintage"] = "2023"
accounting_converterunitsOfix = accounting_converterunitsOfix.rename(
    columns={"technology": "storage_techs"}
)
accounting_converterunitsOfix = accounting_converterunitsOfix[
    [
        "indicator",
        "accNodesData",
        "storage_techs",
        "vintage",
        "perUnitTotal",
    ]
]

accounting_converterUnitse2p = pd.DataFrame()
accounting_converterUnitse2p["storage_techs"] = pd.Series(storage_techs)
accounting_converterUnitse2p["accNodesData"] = "global"
accounting_converterUnitse2p["indicator"] = (
    "e2p_" + accounting_converterUnitse2p["storage_techs"]
)
accounting_converterUnitse2p["storage_techs"] = (
    accounting_converterUnitse2p["storage_techs"] + "_charger"
)
accounting_converterUnitse2p["vintage"] = "2023"
accounting_converterUnitse2p["perUnitTotal"] = 1
# %%
accounting_converterUnits = (
    pd.concat(
        [
            accounting_converterunitsInv,
            accounting_converterunitsOfix,
            accounting_converterUnitse2p,
        ]
    )
    .set_index(["indicator", "accNodesData", "storage_techs", "vintage"])
    .fillna(0)
)
m.parameter.add(accounting_converterUnits, "accounting_converterunits")
accounting_converterUnits
# %% [markdown]
# ## Demand
#
# To account for the electricity demand we use the load signal from the
# original dataset. We handle it similarly to the renewable energy profiles. We
# divide by 1000 to convert the unit from MWh to GWh. First, we load and format
# the data.
# %%
raw_sourcesinkprofile = pd.read_csv(
    os.path.join(raw_folder, "Nodal_TS/load_signal.csv"), parse_dates=["Time"]
)
raw_sourcesinkprofile_2013 = raw_sourcesinkprofile[
    raw_sourcesinkprofile.loc[:, "Time"].dt.year == 2013
]
raw_sourcesinkprofile_2013.loc[:, "Time"] = raw_sourcesinkprofile_2013.loc[
    :, "Time"
].apply(lambda x: "t" + str(int(hour_of_year(x)) + 1).zfill(4))
raw_sourcesinkprofile_2013 = raw_sourcesinkprofile_2013.set_index("Time").iloc[:8760].T
raw_sourcesinkprofile_2013.index.name = "node"
raw_sourcesinkprofile_2013 = raw_sourcesinkprofile_2013.div(1e3)
# %% [markdown]
# We are adding the information necessary for REMix sink profiles.
# %%
sourcesink_profile = raw_sourcesinkprofile_2013.reset_index()
sourcesink_profile["nodesData"] = sourcesink_profile["node"].astype(str).str.zfill(4)
sourcesink_profile = sourcesink_profile.reset_index().drop(columns=["node", "index"])
sourcesink_profile["nodesData"] = "ID" + sourcesink_profile["nodesData"].astype(str)
sourcesink_profile = sourcesink_profile[
    sourcesink_profile["nodesData"].isin(
        list(m.map.aggregatenodesmodel.index.levels[0])
    )
]
sourcesink_profile["years"] = "2023"
sourcesink_profile["type"] = "fixed"
sourcesink_profile["techs"] = "Demand"
sourcesink_profile["commodity"] = "Elec"
sourcesink_profile = sourcesink_profile.set_index(
    ["nodesData", "years", "techs", "commodity", "type"]
)
sourcesink_profile = -sourcesink_profile.round(3)
if len(REDUCED_SCOPE_DATA) > 0:
    sourcesink_profile = sourcesink_profile.query("nodesData  == @REDUCED_SCOPE_DATA")
m.profile.add(sourcesink_profile, "sourcesink_profile")
sourcesink_profile.iloc[:, 0:8]
# %% [markdown]
# In REMix we need to explicitly state that a profile or an upper limit has to
# be used to calculate the flows from sources and to sinks. This is done using
# the `sourcesink_config` table.
# %%
sourcesink_config = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [network_nodes, m.set.yearssel, ["Demand"], ["Elec"]]
    )
)
sourcesink_config.loc[idx[:, :, :, :], "usesFixedProfile"] = 1
sourcesink_config.dropna(inplace=True)

m.parameter.add(sourcesink_config, "sourcesink_config")
sourcesink_config
# %% [markdown]
# ## Storage technical parameters
#
# The storage technical parameters are defined in the reference dataset.
# %%
storage_techParam = storage_reference_parameters[["technology", "lifetime"]].rename(
    columns={"technology": "storage_techs", "lifetime": "lifeTime"}
)
storage_techParam["vintage"] = "2023"
storage_techParam["levelUpperLimit"] = 1
storage_techParam = storage_techParam.set_index(["storage_techs", "vintage"])
m.parameter.add(storage_techParam, "storage_techparam")
storage_techParam
# %%
storage_sizeParam = storage_reference_parameters[["technology", "e2p_ratio"]].rename(
    columns={"technology": "storage_techs", "e2p_ratio": "size"}
)
storage_sizeParam["vintage"] = "2023"
storage_sizeParam["commodity"] = "Storage_" + storage_sizeParam["storage_techs"]
storage_sizeParam = storage_sizeParam.set_index(
    ["storage_techs", "vintage", "commodity"]
)
m.parameter.add(storage_sizeParam, "storage_sizeparam")
storage_sizeParam
# %% [markdown]
# The construction of new storage units will be unconstrained.
# %%
storage_techs = ["LFP", "NMC", "LeadAcid", "RedoxFlow"]
storage_reservoirParam = pd.DataFrame(
    index=pd.MultiIndex.from_product([network_nodes, ["2023"], storage_techs])
)
storage_reservoirParam.index.names = ["nodesData", "vintage", "storage_techs"]
storage_reservoirParam["unitsLowerLimit"] = 0
storage_reservoirParam["unitsUpperLimit"] = np.inf
m.parameter.add(storage_reservoirParam, "storage_reservoirparam")
storage_reservoirParam
# %% [markdown]
# Accounting values can be extracted from the reference dataset.
# Cost values are in k€.
# %%
accounting_storageUnitsInv = storage_reference_parameters.loc[
    :, ["technology", "storage_investment", "e2p_ratio", "lifetime"]
]
accounting_storageUnitsInv["perUnitBuild"] = (
    accounting_storageUnitsInv["storage_investment"]
    * accounting_storageUnitsInv["e2p_ratio"]
    * 1000
)
accounting_storageUnitsInv["accNodesData"] = "global"
accounting_storageUnitsInv["indicator"] = "Invest"
accounting_storageUnitsInv["vintage"] = "2023"
accounting_storageUnitsInv["useAnnuity"] = 1
accounting_storageUnitsInv["interest"] = 0.06
accounting_storageUnitsInv = accounting_storageUnitsInv.rename(
    columns={"technology": "storage_techs", "lifetime": "amorTime"}
)
accounting_storageUnitsInv = accounting_storageUnitsInv[
    [
        "indicator",
        "accNodesData",
        "storage_techs",
        "vintage",
        "perUnitBuild",
        "useAnnuity",
        "amorTime",
        "interest",
    ]
]

accounting_storageUnitsOfix = storage_reference_parameters.loc[
    :, ["technology", "storage_investment", "e2p_ratio", "lifetime", "om_cost"]
]
accounting_storageUnitsOfix.loc[:, "perUnitTotal"] = (
    accounting_storageUnitsOfix.loc[:, "storage_investment"]
    * accounting_storageUnitsOfix.loc[:, "e2p_ratio"]
    * 1000
    * accounting_storageUnitsOfix.loc[:, "om_cost"]
    / 100
)

accounting_storageUnitsOfix["accNodesData"] = "global"
accounting_storageUnitsOfix["indicator"] = "OMFix"
accounting_storageUnitsOfix["vintage"] = "2023"
accounting_storageUnitsOfix = accounting_storageUnitsOfix.rename(
    columns={"technology": "storage_techs"}
)
accounting_storageUnitsOfix = accounting_storageUnitsOfix[
    [
        "indicator",
        "accNodesData",
        "storage_techs",
        "vintage",
        "perUnitTotal",
    ]
]

accounting_storageUnitse2p = pd.DataFrame()
accounting_storageUnitse2p["storage_techs"] = pd.Series(storage_techs)
accounting_storageUnitse2p["accNodesData"] = "global"
accounting_storageUnitse2p["indicator"] = (
    "e2p_" + accounting_storageUnitse2p["storage_techs"]
)
accounting_storageUnitse2p["vintage"] = "2023"
accounting_storageUnitse2p["perUnitTotal"] = -1
# %%
accounting_storageUnits = (
    pd.concat(
        [
            accounting_storageUnitsInv,
            accounting_storageUnitsOfix,
            accounting_storageUnitse2p,
        ]
    )
    .set_index(["indicator", "accNodesData", "storage_techs", "vintage"])
    .fillna(0)
)
m.parameter.add(accounting_storageUnits.round(3), "accounting_storageunits")
accounting_storageUnits
# %% [markdown]
# ## Adding transfer line information
#
# We include the electricity transmission infrastructure data from the static
# data.
# %%
base = os.path.join(raw_folder, "Static_data/")
df_raw = pd.read_csv(os.path.join(base, "network_edges.csv"), delimiter=",")
df_raw = df_raw.reset_index().set_index("index")
df_raw
# %% [markdown]
# We use the node names as a filter for the edge data.
# %%
df_regions = pd.read_csv(os.path.join(base, "network_nodes.csv"), delimiter=",")
if len(REDUCED_SCOPE_DATA) == 0:
    filterIDs = df_regions["ID"].tolist()
else:
    filterIDs = [
        int(r.replace("ID", "").lstrip("0")) for r in REDUCED_SCOPE_DATA if "ID" in r
    ]
df_filtered = df_raw[df_raw.loc[:, ("fromNode", "toNode")].isin(filterIDs).all(axis=1)]
# df_filtered = df_raw
df_filtered["fromNode"] = "ID" + df_filtered["fromNode"].astype(str).str.zfill(4)
df_filtered["toNode"] = "ID" + df_filtered["toNode"].astype(str).str.zfill(4)
df_filtered = df_filtered.reset_index()
df_filtered["index"] = "L" + df_filtered["index"].astype(str)
df_filtered = df_filtered.set_index("index")
# %% [markdown]
# To assign the start and end of an edge we define a connections table.
# %%
transfer_connections_1 = pd.DataFrame(df_filtered["fromNode"])
transfer_connections_1["start"] = 1
transfer_connections_1.rename(columns={"fromNode": "node"}, inplace=True)
transfer_connections_1.fillna(0.0, inplace=True)
transfer_connections_2 = pd.DataFrame(df_filtered["toNode"])
transfer_connections_2["end"] = 1
transfer_connections_2.rename(columns={"toNode": "node"}, inplace=True)
transfer_connections_2.fillna(0.0, inplace=True)
transfer_connections = pd.concat(
    [transfer_connections_1, transfer_connections_2], axis=0
)
transfer_connections.fillna(0.0, inplace=True)
transfer_connections.sort_index(inplace=True)
transfer_connections.set_index("node", append=True, inplace=True)
# %% [markdown]
# We remove possible errors by not allowing start and end to be the same node.
# %%
transfer_sum = transfer_connections.groupby(["index"]).aggregate(
    {"start": "sum", "end": "sum"}
)
transfer_error = list(
    transfer_sum[(transfer_sum["start"] == 1) & (transfer_sum["end"] == 1)].index
)
transfer_connections = transfer_connections.reset_index()
transfer_connections = transfer_connections[
    transfer_connections["index"].isin(transfer_error)
]
transfer_connections = transfer_connections.set_index(["index", "node"])
m.parameter.add(transfer_connections, "transfer_linkstartend")
transfer_connections
# %% [markdown]
# Assign length to the links.
# %%
transfer_lengthParam = pd.DataFrame(df_filtered["length"])
transfer_lengthParam["linktype"] = "land"
transfer_lengthParam.set_index("linktype", append=True, inplace=True)
transfer_lengthParam
m.parameter.add(transfer_lengthParam.round(2), "transfer_lengthparam")
# %% [markdown]
# Assign number of lines per connection.
# %%
transfer_linksParam = df_filtered.loc[:, ("numLines", "limit")]
transfer_linksParam["year"] = "2023"
transfer_linksParam["tech"] = "AC"
transfer_linksParam.rename(
    columns={"numLines": "circuits", "limit": "linksUpperLimit"}, inplace=True
)
transfer_linksParam.set_index(["year", "tech"], append=True, inplace=True)
transfer_linksParam["linksUpperLimit"] = transfer_linksParam["linksUpperLimit"].div(1e3)
transfer_linksParam
m.parameter.add(transfer_linksParam, "transfer_linksparam")
# %% [markdown]
# Also technical parameters.
# %%
transfer_techParam = pd.DataFrame(
    index=pd.MultiIndex.from_product([["AC"], m.set.yearssel])
)
transfer_techParam.loc[:, "lifeTime"] = 40
transfer_techParam.loc[:, "flowUpperLimit"] = 1
m.parameter.add(transfer_techParam, "transfer_techparam")
transfer_techParam
# %%
transfer_coefficient = pd.DataFrame(
    index=pd.MultiIndex.from_product([["AC"], m.set.yearssel, ["Elec"]])
)
transfer_coefficient["coefficient"] = 1  # GWh / h

m.parameter.add(transfer_coefficient, "transfer_coefficient")
transfer_coefficient
# %%
# %% [markdown]
# ## Adding annual sum sources and sinks
#
# We add `slack` as a source representing the cost of importing electricity
# from outside the system. We limit the annual sum of fuel imports into a model
# region.
# %%
sourcesink_annualSum = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [network_nodes, m.set.yearssel, ["slack"], ["Elec"]]
    )
)
sourcesink_annualSum.loc[idx[:, :, :, :], "upper"] = np.inf
sourcesink_annualSum.loc[idx[:, :, :, :], "lower"] = 0
sourcesink_annualSum.dropna(inplace=True)

m.parameter.add(sourcesink_annualSum, "sourcesink_annualsum")
sourcesink_annualSum
# %%
# "sourcesink_config" (import configuration)
sourcesink_config = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [network_nodes, m.set.yearssel, ["slack"], ["Elec"]]
    )
)
sourcesink_config.loc[idx[:, :, :, :], "usesUpperSum"] = 1
sourcesink_config.loc[idx[:, :, :, :], "usesLowerProfile"] = 1
sourcesink_config.dropna(inplace=True)

m.parameter.add(sourcesink_config, "sourcesink_config")
sourcesink_config
# %%
# setting a cost for methane imports
accounting_sourcesinkFlow = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [["slackCost"], ["global"], m.set.yearssel, ["slack"], ["Elec"]]
    )
)
accounting_sourcesinkFlow["perFlow"] = 760000  # Mio EUR per GWh_ch CH4

m.parameter.add(accounting_sourcesinkFlow, "accounting_sourcesinkflow")
accounting_sourcesinkFlow
# %% [markdown]
# We add `emissionSink` to allow the model to emit CO2. We allow infinite
# emissions but add a limit in the accounting section.
# %%
sourcesink_annualSumCO2 = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [m.set.nodesdata, m.set.yearssel, ["emissionSink"], ["CO2"]]
    )
)
sourcesink_annualSumCO2.loc[idx[:, :, :, :], "upper"] = 0
sourcesink_annualSumCO2.loc[idx[:, :, :, :], "lower"] = -np.inf
sourcesink_annualSumCO2.dropna(inplace=True)

m.parameter.add(sourcesink_annualSumCO2, "sourcesink_annualsum")
sourcesink_annualSumCO2
# %%
sourcesink_configCO2 = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [m.set.nodesdata, m.set.yearssel, ["emissionSink"], ["CO2"]]
    )
)
sourcesink_configCO2.loc[idx[:, :, :, :], "usesLowerSum"] = 1
sourcesink_configCO2.loc[idx[:, :, :, :], "usesUpperProfile"] = 1
sourcesink_config.dropna(inplace=True)

m.parameter.add(sourcesink_configCO2, "sourcesink_config")
sourcesink_configCO2
# %%
accounting_sourcesinkFlowCO2 = pd.DataFrame(
    index=pd.MultiIndex.from_product(
        [
            ["emissionCost", "carbon"],
            ["global"],
            m.set.yearssel,
            ["emissionSink"],
            ["CO2"],
        ]
    )
)
accounting_sourcesinkFlowCO2.loc[idx["emissionCost", :, :, :], "perFlow"] = (
    -50
)  # Mio EUR per Mton CO2
accounting_sourcesinkFlowCO2.loc[idx["carbon", :, :, :], "perFlow"] = -1
m.parameter.add(accounting_sourcesinkFlowCO2, "accounting_sourcesinkflow")
accounting_sourcesinkFlowCO2
# %% [markdown]
# For now we assign the criticality index to the dataset but it is ignored in
# the base model. To use it, one has to configure it as a target variable.
# %%
m.set.indicators.extend(
    ["Criticality", "LFP_Crit", "NMC_Crit", "LeadAcid_Crit", "RedoxFlow_Crit"]
)
# %% [markdown]
# Write the files to the data directory:
# %%
m.write(fileformat="csv", map_format="csv", profile_format="tall", support_sets=True)
# %% [markdown]
# Using the following `remix run ...` command the model can be solved.
# For now we comment it out so we can use this file as a script without having
# to solve it. The model can also be solved from the command line using the
# following command:
# ```bash
# remix run --datadir=data
# ```
# %%
# m.run(
#     resultfile="cost",
#     lo=3,
#     postcalc=1,
#     roundts=1,
# )
