# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %% [markdown]
# # NUTS aggregation
#
# This script can be used to add mappings to aggregate the dataset to different
# regional scopes. There are errors related to mapping Balkan countries because
# they are not included in the GISCO data.
# %%
import geopandas as gpd
from remix.framework.api.instance import Instance
import pandas as pd

# %% [markdown]
# We recommend only running this code for the resolution that you need. Pick
# between: "nuts0", "nuts1", "nuts2" and "nuts3".
# %%
LEVEL = "nuts0"
# %%
m = Instance()
# %%[markdown]
# ## Loading and adjusting data
#
# In this script we use the geodata downloaded in the first script
# (`a_get_data.py`).
# This mapping is for reference, all relations are done with Pandas and
# Geopandas.
# We do some data handling, so the format becomes like we need it.
# %%
nuts_map = {
    "POR": "Portugal",
    "ESP": "Spain",
    "FRA": "France",
    "BEL": "Belgium",
    "CHE": "Switzerland",
    "LUX": "Luxembourg",
    "NLD": "Netherlands",
    "ITA": "Italy",
    "DEU": "Germany",
    "AUT": "Austria",
    "DNK": "Denmark",
    "CZE": "Czech Republic",
    "POL": "Poland",
    "HUN": "Hungary",
    "SVK": "Slovakia",
    "SVN": "Slovenia",
    "HRV": "Croatia",
    "GRC": "Greece",
    "ALB": "Albania",
    "MKD": "Republic of Macedonia",
    "BGR": "Bulgaria",
    "MNE": "Montenegro",
    "BIH": "Bosnia and Herzegovina",
    "SRB": "Serbia",
    "ROU": "Romania",
}
map_nodes = pd.read_csv("../data/map_aggregatenodesmodel.csv")
REDUCED_SCOPE_DATA = map_nodes.nodesData.to_list() + ["global"]
# %%
bih_01 = gpd.read_file("../raw/geoBoundaries-BIH-ADM0_simplified.geojson")
kosovo = gpd.read_file("../raw/geoBoundaries-XKX-ADM0_simplified.geojson")
# %%
nuts_regions = gpd.read_file("../raw/NUTS_RG_20M_2021_4326.geojson")
nodes_data_in = pd.read_csv("../raw/Static_data/network_nodes.csv")
nodes_data = gpd.GeoDataFrame(
    nodes_data_in,
    crs=nuts_regions.crs,
    geometry=gpd.points_from_xy(nodes_data_in.longitude, nodes_data_in.latitude),
)
nodes_data["nodesData"] = "ID" + nodes_data["ID"].astype(str).str.zfill(4)
power_plant_in = pd.read_csv("../raw/Static_data/generator_info.csv")
power_plant_data = gpd.GeoDataFrame(
    power_plant_in,
    crs=nuts_regions.crs,
    geometry=gpd.points_from_xy(power_plant_in.longitude, power_plant_in.latitude),
)
power_plant_data["nodesData"] = "GEN" + power_plant_data["ID"].astype(str).str.zfill(5)
# %%
level_0 = nuts_regions[nuts_regions["LEVL_CODE"] == 0]
level_1 = nuts_regions[nuts_regions["LEVL_CODE"] == 1]
level_2 = nuts_regions[nuts_regions["LEVL_CODE"] == 2]
level_3 = nuts_regions[nuts_regions["LEVL_CODE"] == 3]
levels = {"nuts0": level_0, "nuts1": level_1, "nuts2": level_2, "nuts3": level_3}
# %%
kosovo_level = kosovo[["geometry"]]
kosovo_level["nodesModel"] = "KSV"
kosovo_level["CNTR_CODE"] = "KSV"
bih_level = bih_01[["geometry"]].reset_index()
bih_level["nodesModel"] = "BIH"  # + bih_level["index"].astype(str)
bih_level["CNTR_CODE"] = "BIH"
bih_level = bih_level[["nodesModel", "CNTR_CODE", "geometry"]]

nuts_level = levels[LEVEL][["NUTS_ID", "CNTR_CODE", "geometry"]].rename(
    columns={"NUTS_ID": "nodesModel"}
)
level = pd.concat([nuts_level, bih_level, kosovo_level])
# %% [markdown]
# ## Aggregation with Geopandas
#
# Using spatial joins we find all the nodes that are within each region to
# build a map that REMix can read. For offshore wind, instead of looking for
# nodes between, we look for the nearest polygon and associate the node to it.
# The latter method can lead to inconsistencies in countries that have nodes
# very close to another country. The rigorous validation of this process is
# beyond the scope of this first iteration but not to be ignored.
# %%
base_crs = level.crs
onshore_id = nodes_data.sjoin(level, how="left", predicate="within").dropna()
offshore = nodes_data[
    nodes_data.sjoin(level, how="left", predicate="within")["index_right"].isna()
]
offshore_id = (
    offshore.to_crs("EPSG:32633")
    .sjoin_nearest(level.to_crs("EPSG:32633"), how="left")
    .dropna()
    .to_crs(base_crs)
)

power_plant_id = (
    power_plant_data.to_crs(level.crs)
    .sjoin(level, how="left", predicate="within")
    .dropna()
)
powerplant_islands = power_plant_data[
    power_plant_data.to_crs(level.crs)
    .sjoin(level, how="left", predicate="within")["nodesModel"]
    .isna()
]
powerplant_islands_id = (
    powerplant_islands.to_crs("EPSG:32633")
    .sjoin_nearest(level.to_crs("EPSG:32633"), how="left")
    .dropna()
    .to_crs(base_crs)
)
# %%[markdown]
# ## Inclusion in REMix
#
# Similarly to part b we add map information to the model but store it as a
# subscenario. This way we have the possibility of running models in multiple
# resolutions without having to rebuild the data all the time.
# %%
map_agg = pd.concat(
    [
        powerplant_islands_id[["nodesData", "nodesModel"]],
        power_plant_id[["nodesData", "nodesModel"]],
        offshore_id[["nodesData", "nodesModel"]],
        onshore_id[["nodesData", "nodesModel"]],
    ]
)
if len(REDUCED_SCOPE_DATA) > 0:
    map_agg = map_agg[map_agg["nodesData"].isin(REDUCED_SCOPE_DATA)]
map_agg = map_agg.set_index(["nodesData", "nodesModel"])
map_agg["aggregate"] = ""
map_agg.columns = [""]

m.map.add(map_agg, "aggregatenodesmodel")
m.set.add(
    list(sorted(set(m.map.aggregatenodesmodel.index.get_level_values(1)))),
    "nodesmodelsel",
)
m.datadir = f"../data/{LEVEL}"
m.write(fileformat="csv", map_format="csv")
m.datadir = f"../data/criticality/{LEVEL}"
m.write(fileformat="csv", infer=False, map_format="csv")
# %%
