# %%
# SPDX-FileCopyrightText: Copyright (c) 2024 German Aerospace Center (DLR)
# SPDX-License-Identifier: BSD-3-Clause
# %%
from dash import Dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import plotly.express as px
import json
from remix.framework.tools.gdx import GDXEval
import pandas as pd

idx = pd.IndexSlice
# %%
result_dir = "results"
results = GDXEval(f"{result_dir}/criticality_martens.gdx")
# %%
# %%
with open("../raw/NUTS_RG_20M_2021_4326.geojson") as json_file:
    geojson = json.load(json_file)
nuts_map = {
    "POR": "Portugal",
    "ESP": "España",
    "FRA": "France",
    "BEL": "Belgique/België",
    "CHE": "Schweiz/Suisse/Svizzera",
    "LUX": "Luxembourg",
    "NLD": "Nederland",
    "ITA": "Italia",
    "DEU": "Deutschland",
    "AUT": "Österreich",
    "DNK": "Danmark",
    "CZE": "Česko",
    "POL": "Polska",
    "HUN": "Magyarország",
    "SVK": "Slovensko",
    "SVN": "Slovenija",
    "HRV": "Hrvatska",
    "GRC": "Elláda",
    "ALB": "Shqipëria",
    "MKD": "Severna Makedonija",
    "BGR": "Bulgaria",
    "MNE": "Crna Gora",
    "BIH": "BIH",
    "SRB": "Serbia",
    "ROU": "România",
}
# %%
generation_values = (
    results["commodity_balance_annual"]
    .loc[idx[:, :, :, :, "Elec", "positive"], idx[:]]
    .groupby(["pareto", "accNodesModel", "techs"], observed=False)
    .sum()
    .div(1e3)
    .round(0)
    .unstack()
)
generation_values.columns = generation_values.columns.get_level_values(1)
generation_values.columns.name = None
generation_values = generation_values.reset_index()
generation_values["Generation"] = 0
generation_values["accNodesModel"] = generation_values["accNodesModel"].apply(
    lambda x: nuts_map.get(x, x)
)
generation_values_europe = generation_values[
    (generation_values["accNodesModel"] != "global")
]
# %%
technologies = (
    generation_values_europe.iloc[:, 2:]
    .sum()
    .reset_index()
    .rename(columns={0: "sm"})
    .query("sm > 0")["index"]
    .values
)
generation_values_europe = generation_values_europe[
    ["pareto", "accNodesModel"] + list(technologies)
]
# %%
figure_map = {}
for t in technologies:
    figure_map[t] = fig = px.choropleth(
        data_frame=generation_values_europe,
        geojson=geojson,
        locations=generation_values_europe.accNodesModel,
        color="{}".format(t),
        center={"lat": 48, "lon": 10},
        range_color=[0, generation_values_europe[f"{t}"].max()],
        color_continuous_scale="viridis",
        featureidkey="properties.NAME_LATN",
        animation_frame="pareto",
        scope="europe",
        width=800,
        height=600,
        hover_data=["pareto", "accNodesModel", "{}".format(t)],
    )
# %% [markdown]
# ## App layout
#
# What goes inside dash layout are the dash components and any html we need.
# %%
app = Dash("Map")
app.layout = html.Div(
    [
        html.H1("Generation in TWh", style={"text-align": "center"}),  # html Title
        html.Div(id="metric_title", children=["Metric"], style={"font-weight": "bold"}),
        dcc.Dropdown(
            id="technology",
            placeholder="Select Metric",
            options=technologies,
            value="PV",
            style={
                "width": "60%",
            },
        ),
        html.Br(),  # space between dropdowns
        # create div element, e.g., text to display dropdown selection
        html.Br(),  # space
        # graph object, e.g., choropleth
        dcc.Graph(id="choropleth", figure={}),
    ]
)


# Connect the Plotly graphs with Dash Components
@app.callback(
    Output(component_id="choropleth", component_property="figure"),
    Input(component_id="technology", component_property="value"),
)
def update_graph(technology):
    # Plotly Express
    fig = figure_map[technology]
    return fig


app.run()
