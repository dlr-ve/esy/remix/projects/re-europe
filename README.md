# RE-Europe REMix

These scripts allow building and evaluating a REMix model based on the
RE-Europe dataset. It also has a structure that allows the evaluation of a
multi-objective optimization using REMix features. It serves as a tutorial on
how to build a model using open data. 

Source: [Data Descriptor: RE-Europe, a large-scale dataset for modelling a
highly renewable European electricity
system](https://doi.org/10.1038/sdata.2017.175)

Data: [zenodo](https://doi.org/10.5281/zenodo.620228)

## Requirements

To build and run the model you require python higher than 3.7 and lower than 3.12 with the dependencies
listed in the requirements.txt file. REMix has the additional dependency of
GAMS 37 or above which is a proprietary software for which a licence is needed.

To be able to run the notebooks is strongly adviced to create and activate an
environment with REMix installed as instruced at the [REMix
documentation](https://gitlab.com/dlr-ve/esy/remix/framework).

With the activated environment then is adviced to install the requirements using conda:

```bash
conda env update --file env.yml --prune
```

You can also create your environment simply by doing:


```bash
conda env create -f env.yml
```

If using other environment manager, the requirements can be installed with pip:


```bash
pip install -r requirements.txt
```


## Running

Just run the scripts using the interface of your IDE or call python like:

```bash
cd src
python <scriptname>
```

## Build Notebooks

Jupyter notebooks are great but they are not very friendly with git diff. We
suggest working with VSCode or Spyder which let you run marked cells. Or
install [`jupytext`](https://github.com/mwouts/jupytext) and run on bash:

```bash
jupytext --sync src/*.py
```

---
**Note**

This command will work only on unix-like environments. Jupytext has problems with windows.

---

In windows call:

```powershell
jupytext --to ipynb src/*.py
```

This command will create the notebooks next to the .py files.

## Acknowledgements

These scripts are produced in context of the project RESUME with the grant number 03EI1048A, financed by the German Ministry of Economic Affairs and Climate Action.

## References

Jensen, T. V., de Sevin, H., Greiner, M., & Pinson, P. (2017). The RE-Europe data set [Data set]. Zenodo. https://doi.org/10.5281/zenodo.999150

Runfola, D. et al. (2020) geoBoundaries: A global database of political administrative boundaries. PLoS ONE 15(4): e0231866. https://doi.org/10.1371/journal.pone.0231866

## Copyright notices

© EuroGeographics for the administrative boundaries in the European Union

## Contact

Eugenio Salvador Arellano Ruiz: eugenio.arellanoruiz@dlr.de
